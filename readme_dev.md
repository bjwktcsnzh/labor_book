# 开发者文档

## 源神，启动！

你需要：

- Node.js 版本 18 或 更高.
- 一个支持 Markdown 格式的文本编辑器.
  - 推荐使用 VSCode ， 因为它还能写 Vue 扩展 .

```shell
npm i --loglevel verbose
```

## 排错

### Windows Powershell Vscode Terminal 中文乱码

原因: pino 输出编码为 utf-8 的日志文本到 windows gbk 控制台。

解决方法:

```powershell
chcp 65001
```

### Unknown file extension ".ts"

`npm run dev` 时报错:

```
TypeError [ERR_UNKNOWN_FILE_EXTENSION]: Unknown file extension ".ts"
```

解决方法:

https://github.com/vitejs/vite/issues/5370#issuecomment-1339022262
