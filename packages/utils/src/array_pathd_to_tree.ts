import { _has_property } from "./typed";
import { WithFlag, PartialRecord, find_first_else } from "./util";

export type PathdTreeNode<
  PathFlag extends string,
  Param extends WithFlag<PathFlag>,
  NameFlag extends string,
  ItemsFlag extends string,
  ParamFlag extends string,
  SUBTYPE extends PathdTreeNode<
    PathFlag,
    Param,
    NameFlag,
    ItemsFlag,
    ParamFlag,
    SUBTYPE
  >
> = Record<NameFlag, string> &
  Record<ItemsFlag, SUBTYPE[]> &
  PartialRecord<ParamFlag, Param>;

export async function array_pathd_to_tree<
  PathFlag extends string,
  PathItemFlag extends string,
  ItemsFlag extends string,
  ParamFlag extends string,
  Param extends WithFlag<PathFlag>,
  N extends PathdTreeNode<
    PathFlag,
    Param,
    PathItemFlag,
    ItemsFlag,
    ParamFlag,
    N
  >
>(_args: {
  split_char: string;
  params: Param[];
  flags: Record<"path", PathFlag> &
    Record<"path_item", PathItemFlag> &
    Record<"items", ItemsFlag> &
    Record<"param", ParamFlag>;
  hooks: {
    onCreateMapping: (_arg: {
      param: Param;
      path_item: string;
      is_param_end: boolean;
    }) => Promise<N>;
    afterMountValue: (n: N, brother_nodes: N[]) => Promise<void>;
  };
}): Promise<N[]> {
  const { params, flags, hooks, split_char } = _args;

  const results = [] as N[];
  let parent: N | null = null;
  for (const param of params) {
    parent = null;
    const param_splited = param[flags.path].split(split_char);
    for (let i = 0; i < param_splited.length; i++) {
      const path_item = param_splited[i];
      const brother_nodes: N[] = parent == null ? results : parent[flags.items];
      const is_param_end = i == param_splited.length - 1;
      const { is_exist, value } = await find_first_else(
        brother_nodes,
        async (it) => it[flags.path_item] == path_item,
        async (arr) => {
          const _n = await hooks.onCreateMapping({
            param,
            path_item,
            is_param_end,
          });
          arr.push(_n);
          return _n;
        }
      );
      parent = value;
    }
    if (parent != null) {
      const brother_nodes: N[] = parent == null ? results : parent[flags.items];

      parent[flags.param] = param as any; // TODO: fixed `as any`
      await hooks.afterMountValue(parent, brother_nodes);
    }
  }

  return results;
}

export type ArrayPathToTreeDefaultNode<T> = {
  name: string;
  items: ArrayPathToTreeDefaultNode<T>[];
  param: T;
};

// default argument
export async function array_pathd_to_tree_0<
  PathFlag extends string,
  T extends WithFlag<PathFlag>
>(path_flag: PathFlag, params: T[]) {
  return await array_pathd_to_tree({
    params,
    flags: {
      path: path_flag,
      path_item: "path_item",
      items: "items",
      param: "param",
    },
    hooks: {
      onCreateMapping: async ({ param, path_item, is_param_end }) => {
        return {
          path_item,
          items: [],
          ...(is_param_end ? { param } : {}),
        };
      },
      afterMountValue: async (n) => {},
    },
    split_char: "/",
  });
}
