export function parse_prefix_number<T>(
  text: string,
  split_chars: string[],
  result_callback: (_res: { suffix_idx: number; prefix_numbers: number[] }) => T
): T {
  const prefix_numbers = [] as number[];
  let buf = "";
  function write() {
    if (buf == "") {
      return;
    }
    const num = parseInt(buf);
    if (isNaN(num)) {
      throw new Error(`BUG. text=${text}`);
    }
    prefix_numbers.push(num);
    buf = "";
  }

  for (let i = 0; i < text.length; i++) {
    const ch = text[i];
    if (!isNaN(parseInt(ch))) {
      buf += ch;
    } else if (split_chars.includes(ch)) {
      write();
    } else {
      write();
      return result_callback({
        suffix_idx: i,
        prefix_numbers,
      });
    }
  }
  write();
  return result_callback({
    suffix_idx: text.length,
    prefix_numbers,
  });
}

export function ignore_suffix_zero(arr: readonly number[]) {
  let i = arr.length - 1;
  for (; i >= 0; i--) {
    if (arr[i] != 0) {
      break;
    }
  }
  return arr.slice(0, i + 1);
}
