export type WithFlag<FlagName extends string> = {} & Record<FlagName, string>;
export type PartialRecord<K extends keyof any, T> = Partial<Record<K, T>>;
export type ArrayElement<ArrayType extends readonly unknown[]> =
  ArrayType extends readonly (infer ElementType)[] ? ElementType : never;
// https://stackoverflow.com/a/59906630
export type ArrayLengthMutationKeys =
  | "splice"
  | "push"
  | "pop"
  | "shift"
  | "unshift";
export type FixedLengthArray<
  T,
  L extends number,
  TObj = [T, ...Array<T>]
> = Pick<TObj, Exclude<keyof TObj, ArrayLengthMutationKeys>> & {
  readonly length: L;
  [I: number]: T;
  [Symbol.iterator]: () => IterableIterator<T>;
};

export function _replace_prefix(target: string, prefix: string, repto: string) {
  if (!target.startsWith(prefix)) {
    throw Error(
      `the string require start with prefix "${prefix}" ( replace to "${repto}") , but not , it's "${target}" .`
    );
  }
  return repto + target.substring(prefix.length);
}

export function _replace_suffix(target: string, suffix: string, repto: string) {
  if (!target.endsWith(suffix)) {
    throw Error(
      `the string require ends with suffix "${suffix}" ( replace to "${repto}") , but not , it's "${target}" .`
    );
  }
  return target.substring(0, target.length - suffix.length) + repto;
}

export function find_title_in_markdown_content(content: string) {
  const _tmp_title = content.split("\n").find((line) => line.startsWith("# "));
  if (!_tmp_title) {
    throw Error("Title not found !");
  }
  return _replace_prefix(_tmp_title, "# ", "").trim();
}

export function _if_absent_add_suffix(target: string, suffix: string) {
  if (target.endsWith(suffix)) {
    return target;
  } else {
    return target + suffix;
  }
}

export function _if_absent_remove_prefix(target: string, prefix: string) {
  if (target.startsWith(prefix)) {
    return _replace_prefix(target, prefix, "");
  } else {
    return target;
  }
}

export async function find_first_else<T, R>(
  arr: T[],
  predicate: (value: T, index: number, obj: T[]) => Promise<boolean>,
  on_not_found: (obj: T[]) => Promise<R>
): Promise<
  { is_exist: true; idx: number; value: T } | { is_exist: false; value: R }
> {
  const arr_length = arr.length;
  for (let i = 0; i < arr_length; i++) {
    const value = arr[i];
    if (await predicate(value, i, arr)) {
      return {
        is_exist: true,
        idx: i,
        value,
      };
    }
  }
  return {
    is_exist: false,
    value: await on_not_found(arr),
  };
}

export function find_max(arr: number[]): {
  max_idx: number[];
  num_max: number;
  arr: number[];
} | null {
  if (arr.length == 0) {
    return null;
  }
  let num_max = arr[0];
  let max_idx = [0];

  for (let i = 1; i < arr.length; i++) {
    const num = arr[i];
    if (num < num_max) {
      continue;
    } else if (num > num_max) {
      num_max = num;
      max_idx = [i];
    } else {
      max_idx.push(i);
    }
  }

  return {
    max_idx,
    num_max,
    arr,
  };
}

// Warning : arr can be not matrix
export function transposing_array2d<T>(
  arr: T[][],
  padding_value: (row: number, col: number) => T
): T[][] {
  // Example :
  // (padding_value is null)
  //
  // a1 a2          a1   b1   c1 d1
  // b1        ===> a2   null c2 d2
  // c1 c2 c3       null null c3 null
  // d1 d2

  const result_max_length = find_max(arr.map((it) => it.length));
  if (result_max_length == null) {
    // arr is empty
    return [];
  }
  const max_height = arr[result_max_length.max_idx[0]].length;
  const result = [];
  for (let row = 0; row < max_height; row++) {
    result.push(
      arr.map((it, col) =>
        row >= it.length ? padding_value(row, col) : it[row]
      )
    );
  }
  return result;
}

export function longest_common_prefix(strs: string[]) {
  if (strs.length == 0) return "";
  let ans = strs[0];
  for (let i = 1; i < strs.length; i++) {
    let j = 0;
    for (; j < ans.length && j < strs[i].length; j++) {
      if (ans[j] != strs[i][j]) break;
    }
    ans = ans.slice(0, j);
    if (ans === "") return ans;
  }
  return ans;
}
