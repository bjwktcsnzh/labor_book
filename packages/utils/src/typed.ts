// https://stackoverflow.com/a/68998517/21185704
export function _has_property<
  O,
  P extends PropertyKey,
  V extends unknown // TODO : fixed type predicates
>(obj: O, prop: P): obj is O & Record<P, V>{
  return Object.prototype.hasOwnProperty.call(obj, prop);
}
