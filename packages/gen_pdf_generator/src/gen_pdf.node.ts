import fs from "fs/promises";
import path from "path";
import { _exist_pth, _kill_child_subprocess } from "./util.node";
import fg from "fast-glob";
import {
  _replace_prefix,
  _replace_suffix,
  _if_absent_add_suffix,
  find_title_in_markdown_content,
} from "@labor-book/utils/src/util";
import lodash from "lodash";
import { PDFDocumentProxy, getDocument } from "pdfjs-dist/legacy/build/pdf.mjs";
import matter from "gray-matter";
import { PDFPageProxy } from "pdfjs-dist/types/web/interfaces";
import { IndexingBookContext, _indexing_book } from "./indexing_pdf.node";
import { MainContext } from "./main.node";
import { PackageBookContext, _package_book } from "./package_pdf.node";
import {
  create_browser,
  puppeteer_concurrent_limiter,
} from "./puppeteer_util.node";
import { PDFDocument, PDFName, PDFObject, PDFPage } from "pdf-lib";
import { logger } from "packages/utils/src/log";
import { generate_text_position, init_jar } from "./text_position.node";
import package_json from "../../../package.json";

export async function _generate_pdf(
  ctx_main: MainContext,
  bookname: string,
  output_pdf_cache_dir_name: string
) {
  const output_pdf_cache_dir_suffixed = `${ctx_main.cache_dir_suffixed}${output_pdf_cache_dir_name}/`;
  const docs_filepaths = await fg(
    `${ctx_main.docs_dir_suffixed}${bookname}/**/*.md`
  );
  await init_jar();
  await Promise.all(
    docs_filepaths.map((doc_filepath) => {
      const urlpath_no_suffix = _replace_suffix(
        _replace_prefix(doc_filepath, ctx_main.docs_dir_suffixed, ""),
        ".md",
        ""
      );
      return puppeteer_concurrent_limiter.schedule(() =>
        _generate_pdf_by_urlpath_without_ext__and__init_outline_meta(
          ctx_main,
          urlpath_no_suffix,
          output_pdf_cache_dir_suffixed
        )
      );
    })
  );

  await _init_doc_meta_for_all({
    ctx_main,
    bookname,
    mode: "md",
    output_pdf_cache_dir_suffixed,
  });

  for (const src_pdf_filepath of await fg(
    `${ctx_main.docs_dir_suffixed}${bookname}/**/*.pdf`
  )) {
    const cp_dist_path = _replace_prefix(
      src_pdf_filepath,
      ctx_main.docs_dir_suffixed,
      output_pdf_cache_dir_suffixed
    );
    await fs.copyFile(src_pdf_filepath, cp_dist_path);
  }

  await _init_doc_meta_for_all({
    ctx_main,
    bookname,
    mode: "pdf",
    output_pdf_cache_dir_suffixed,
  });
  const ctx = new IndexingBookContext();
  await _indexing_book(
    ctx_main,
    ctx,
    bookname,
    `${output_pdf_cache_dir_suffixed}${bookname}/`
  );

  await _package_book(
    new PackageBookContext({
      ctx_main,
      ctx_index: ctx,
      bookname,
      output_pdf_cache_dir_suffixed,
    })
  );

  logger.info(
    `
    +---------------------------------------------------------+
    |
    |   Success generate PDF for book 《${bookname}》
    |
    +---------------------------------------------------------+
    `
  );
}

async function _generate_pdf_by_urlpath_without_ext__and__init_outline_meta(
  ctx_main: MainContext,
  url_path_no_suffix: string,
  output_pdf_cache_dir_suffixed: string
) {
  logger.debug(
    { url_path_no_suffix, output_pdf_cache_dir_suffixed },
    "Start generate pdf"
  );
  logger.trace({ url_path_no_suffix }, `Start puppeteer`);
  const browser = await create_browser();
  try {
    const page = await browser.newPage();
    const domain_actual = `http://localhost:${ctx_main._obj.preview_server.port}`;
    const domain_fake = package_json.homepage;
    await page.goto(`${domain_actual}/${url_path_no_suffix}.html`, {
      waitUntil: "networkidle2",
    });
    await page.addStyleTag({
      content: await fs.readFile(
        `${ctx_main.styles_dir_suffixed}gen_pdf.css`,
        "utf-8"
      ),
    });
    await page.addScriptTag({
      content: `
      const domain_actual = ${JSON.stringify(domain_actual)};
      const domain_fake = ${JSON.stringify(domain_fake)};

      document.querySelectorAll('details').forEach(it=>it.setAttribute('open',true));

      document.querySelectorAll("a").forEach((it) => {
        if (it["href"] && it["href"].includes(domain_actual)) {
          it["href"] = it["href"].replace(domain_actual, domain_fake);
        }
      });
      `,
    });
    const buf = await page.pdf({
      // TODO : page header and footer
      displayHeaderFooter: false,
      // headerTemplate:'',
      // footerTemplate:'',
      preferCSSPageSize: true,
      printBackground: true,
      format: "A4",
      tagged: true,
      margin: {
        top: "2cm",
        right: "2cm",
        bottom: "2cm",
        left: "2cm",
      },
      timeout: 30 * 1000,
    });
    const out_file = `${output_pdf_cache_dir_suffixed}${url_path_no_suffix}.pdf`;
    if (!(await _exist_pth(path.dirname(out_file)))) {
      await fs.mkdir(path.dirname(out_file), { recursive: true });
    }
    await fs.writeFile(out_file, buf);

    logger.trace(
      {
        url_path_no_suffix,
        out_file,
      },
      "Success generate pdf and init outline meta"
    );
  } catch (err) {
    throw new Error(
      `Error on use puppeteer generate pdf ${url_path_no_suffix}`,
      { cause: err }
    );
  } finally {
    await browser.close();
  }
}

async function _init_doc_meta_for_all(_obj: {
  ctx_main: MainContext;
  bookname: string;
  mode: "pdf" | "md";
  output_pdf_cache_dir_suffixed: string;
}) {
  const { ctx_main, mode, output_pdf_cache_dir_suffixed, bookname } = _obj;
  logger.debug(
    {
      mode,
      bookname,
    },
    "init doc meta for book"
  );

  const pdf_filepaths = await fg(
    `${output_pdf_cache_dir_suffixed}${bookname}/**/*.pdf`
  );

  await Promise.all(
    pdf_filepaths.map(async (pdf_filepath) => {
      switch (mode) {
        case "pdf":
          return await _init_doc_meta({
            pdf_filepath: pdf_filepath,
            output_filepath: `${pdf_filepath}.docmeta.pdf.json`,
            mode,
          });
        case "md":
          return await _init_doc_meta({
            md_filepath: _replace_prefix(
              _replace_suffix(pdf_filepath, ".pdf", ".md"),
              output_pdf_cache_dir_suffixed,
              ctx_main.docs_dir_suffixed
            ),
            output_filepath: `${pdf_filepath}.docmeta.md.json`,
            mode,
          });
      }
    })
  );
}

export async function _init_doc_meta(
  _obj:
    | {
        pdf_filepath: string;
        output_filepath: string;
        mode: "pdf";
      }
    | {
        md_filepath: string;
        output_filepath: string;
        mode: "md";
      }
) {
  const { mode, output_filepath } = _obj;

  switch (mode) {
    case "pdf":
      const { pdf_filepath } = _obj;
      logger.trace({ pdf_filepath, mode }, "init doc meta");
      const doc = await getDocument(pdf_filepath).promise;
      const doc2 = await PDFDocument.load(await fs.readFile(pdf_filepath));
      const docmeta_pdf = await _read_pdf_docmeta(doc, doc2);
      await fs.writeFile(output_filepath, JSON.stringify(docmeta_pdf, null, 2));
      await generate_text_position(pdf_filepath);
      break;
    case "md":
      const { md_filepath } = _obj;
      logger.trace({ md_filepath, mode }, "init doc meta");
      const frontmatter = matter(await fs.readFile(md_filepath, "utf-8"));
      const docmeta_md = {
        frontmatter,
        title: find_title_in_markdown_content(frontmatter.content),
      };
      await fs.writeFile(output_filepath, JSON.stringify(docmeta_md, null, 2));
      break;
  }
}

const _copy = (o: any) => {
  try {
    // return o === undefined || o === null ? null : JSON.parse(JSON.stringify(o));
    return lodash.cloneDeep(o);
  } catch (err) {
    throw new Error(`Failed copy object ! object is ${o}`, { cause: err });
  }
};

export async function _read_pdf_docmeta(
  doc: PDFDocumentProxy,
  doc2: PDFDocument
) {
  const {
    annotationStorage,
    filterFactory,
    numPages,
    fingerprints,
    isPureXfa,
    allXfaHtml,
    loadingParams,
  } = doc;
  doc2.defaultWordBreaks;
  return {
    author: doc2.getAuthor(),
    creationDate: _copy(doc2.getCreationDate()),
    creator: doc2.getCreator(),
    keywords: doc2.getKeywords(),
    modificationDate: _copy(doc2.getModificationDate()),
    pageCount: doc2.getPageCount(),
    pageIndices: _copy(doc2.getPageIndices()),
    producer: doc2.getProducer(),
    subject: doc2.getSubject(),
    title: doc2.getTitle(),
    isEncrypted: doc2.isEncrypted,
    defaultWordBreaks: _copy(doc2.defaultWordBreaks),
    annotationStorage,
    filterFactory,
    numPages,
    fingerprints,
    isPureXfa,
    allXfaHtml,
    loadingParams,
    // async getter
    hasJSActions: _copy(await doc.hasJSActions()),
    attachments: _copy(await doc.getAttachments()),
    calculationOrderIds: _copy(await doc.getCalculationOrderIds()),
    downloadInfo: _copy(await doc.getDownloadInfo()),
    fieldObjects: _copy(await doc.getFieldObjects()),
    jsActions: _copy(await doc.getJSActions()),
    markInfo: _copy(await doc.getMarkInfo()),
    metadata: _copy(await doc.getMetadata()),
    openAction: _copy(await doc.getOpenAction()),
    optionalContentConfig: _copy(await doc.getOptionalContentConfig()),
    pageLayout: _copy(await doc.getPageLayout()),
    pageMode: _copy(await doc.getPageMode()),
    permissions: _copy(await doc.getPermissions()),
    viewerPreferences: _copy(await doc.getViewerPreferences()),
    catalog: _copy(_read_pdf_catalog(doc2)),
    outline: _copy(await doc.getOutline()),
    destinations: _copy(await doc.getDestinations()),
    pages: _copy(
      await (async () => {
        const arr = [];
        for (let i = 1; i <= doc.numPages; i++) {
          arr.push({
            index: i,
            page: await _read_pdf_page(await doc.getPage(i)),
          });
        }
        return arr;
      })()
    ),
    pages2: doc2
      .getPages()
      .map((it) => _read_pdf_page2(it))
      .map((it) => _copy(it)),
  };
}

async function _read_pdf_page(page: PDFPageProxy) {
  const {
    pageNumber,
    rotate,
    ref,
    userUnit,
    view,
    filterFactory,
    isPureXfa,
    stats,
  } = page;
  return {
    pageNumber,
    rotate,
    ref,
    userUnit,
    view,
    filterFactory,
    isPureXfa,
    stats,
    jsActions: _copy(await page.getJSActions()),
    xfa: _copy(await page.getXfa()),
    textContent: _copy(
      await page.getTextContent({
        includeMarkedContent: true,
      })
    ),
    structTree: _copy(await page.getStructTree()),
  };
}

function _read_pdf_page2(page: PDFPage) {
  return {
    artBox: page.getArtBox(),
    bleedBox: page.getBleedBox(),
    cropBox: page.getCropBox(),
    height: page.getHeight(),
    mediaBox: page.getMediaBox(),
    position: page.getPosition(),
    rotation: page.getRotation(),
    size: page.getSize(),
    trimBox: page.getTrimBox(),
    width: page.getWidth(),
    x: page.getX(),
    y: page.getY(),
  };
}

function _read_pdf_catalog(page: PDFDocument) {
  const { catalog } = page;

  return catalog.entries().map((entry) => {
    return [_read_pdf_object(entry[0]), _read_pdf_object(entry[1])];
  });
}

function _read_pdf_object(o: PDFObject) {
  return o.toString();
}
