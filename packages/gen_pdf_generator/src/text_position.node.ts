import child_process from "node:child_process";
import path from "path";
import { _exist_pth, _sub_proc_pipe2std_and_await } from "./util.node";
import fs from "fs/promises";
import { PDFOutlineTo } from "./third_part/marp-team/marp-cli/src/utils/pdf";
import { logger } from "../../utils/src/log";
import lodash from "lodash";

export const text_position_jar_file_pth = [
  ".",
  "java",
  "pdfutil",
  "target",
  "pdfutil-1.0-SNAPSHOT.jar",
];

export async function init_jar() {
  await _sub_proc_pipe2std_and_await(
    child_process.spawn(
      /^win32/.test(process.platform) ? "mvn.cmd" : "mvn",
      ["clean", "enforcer:enforce"],
      {
        cwd: path.resolve(".", "java", "pdfutil"),
      }
    )
  );
  await _sub_proc_pipe2std_and_await(
    child_process.spawn(
      /^win32/.test(process.platform) ? "mvn.cmd" : "mvn",
      ["package"],
      {
        cwd: path.resolve(".", "java", "pdfutil"),
      }
    )
  );
  if (!(await _exist_pth(path.join(...text_position_jar_file_pth)))) {
    throw new Error("jar file not found but maven return zero");
  }
}

export async function generate_text_position(pdf_filepath: string) {
  await _sub_proc_pipe2std_and_await(
    child_process.spawn(/^win32/.test(process.platform) ? "java.exe" : "java", [
      "-jar",
      path.join(...text_position_jar_file_pth),
      pdf_filepath,
    ])
  );
}

export type TextPositionData =
  | {
      page: number;
      text: string;
      x: number;
      y: number;
      pageW: number;
      pageH: number;
    }
  | {
      splitCount: number;
    };

export async function _load_text_position(pdf_filepath: string): Promise<{
  [page: number]: TextPosition;
}> {
  const result_cached = `${pdf_filepath}.docmeta.text_position.cached.json`;
  if (await _exist_pth(result_cached)) {
    return JSON.parse(
      await fs.readFile(result_cached, {
        encoding: "utf-8",
      })
    );
  }

  const target = `${pdf_filepath}.docmeta.text_position.json`;
  const datas = JSON.parse(
    await fs.readFile(target, {
      encoding: "utf-8",
    })
  ) as TextPositionData[];
  const result = {} as Awaited<ReturnType<typeof _load_text_position>>;

  let page = 0;

  for (const data of datas) {
    if ("splitCount" in data) {
      const { textlists } = result[page];
      textlists.push([]);
    } else {
      page = data.page;
      if (!(page in result)) {
        // init creating
        result[page] = {
          page,
          w: data.pageW,
          h: data.pageH,
          textlists: [],
        };
      }
      const { textlists } = result[page];
      if (textlists.length == 0) {
        textlists.push([]);
      }
      const textlist = textlists[textlists.length - 1];
      textlist.push({
        text: data.text,
        x: data.x,
        y: data.y,
      });
    }
  }

  await fs.writeFile(result_cached, JSON.stringify(result, null, 2), {
    encoding: "utf-8",
  });
  return result;
}

export type TextPosition = {
  page: number;
  w: number;
  h: number;
  textlists: {
    text: string;
    x: number;
    y: number;
  }[][];
};

export function _find_xy_for_text(
  tp: TextPosition,
  title: string,
  _page_num_default: number
): PDFOutlineTo {
  function wrap_ret(res: PDFOutlineTo) {
    logger.trace(
      {
        title,
        res,
      },
      `find xy for text`
    );
    return res;
  }

  const { textlists } = tp;
  const not_matched = [] as string[];
  for (let i = 0; i < textlists.length; i++) {
    const textlist = textlists[i];
    const fulltext = textlist.map((it) => it.text).join("");

    // if (fulltext == title) {
    //   const { x, y } = textlist[0];

    //   return wrap_ret({
    //     page: _page_num_default,
    //     x,
    //     y: Math.max(0, tp.h - y + 30),
    //   });
    // }

    // 如果没有匹配的话，可能是因为标题中带有标点符号，因此被分隔为了多个 fulltext 。
    let fulltext_concat = "";
    const title_target = title.replaceAll(" ", "");
    let j = i;
    while (j >= 0 && title_target.endsWith(fulltext_concat)) {
      if (fulltext_concat == title_target) {
        const { x, y } = textlists[j + 1][0];
        return wrap_ret({
          page: _page_num_default,
          x,
          y: Math.max(0, tp.h - y + 30),
        });
      } else {
        fulltext_concat =
          textlists[j--]
            .map((it) => it.text)
            .join("")
            .replaceAll(" ", "") + fulltext_concat;
      }
    }

    // Not matched
    not_matched.push(fulltext);
  }

  logger.warn({ title, not_matched }, "Not found x,y for title");
  return _page_num_default;
}
