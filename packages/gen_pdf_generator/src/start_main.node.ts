import { logger } from "@labor-book/utils/src/log";
import { MainContext, main } from "./main.node";
import package_json from "../../../package.json";

if (process.env.IS_GITLAB_CI) {
  logger.info("Start Main by Gitlab CI");
}

await main(
  new MainContext({
    docs_dir: "./book/",
    dist_dir: package_json.config.pdf.distdir,
    cache_dir: "./packages/gen_pdf_generator/.cache/",
    cache_index_file_relative_path: "output_index.json",
    styles_dir: "./packages/gen_pdf_generator/styles/",
    booknames: ["health58"],
    preview_server: {
      port: package_json.config.pdf.previewserver.port,
    },
    is_test: false,
  })
);
