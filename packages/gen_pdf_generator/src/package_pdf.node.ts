import {
  IndexingBookContext,
  OutlineNode,
  OutlineNodeLevel,
  _load_docmeta,
} from "./indexing_pdf.node";
import { MainContext } from "./main.node";
import pdflib, { Duplex, PDFDocument } from "pdf-lib";
import fs from "fs/promises";
import {
  OutlineNodeWithDisplayTitle,
  PDFOutline,
  setOutline,
} from "./third_part/marp-team/marp-cli/src/utils/pdf";
import { _init_doc_meta } from "./gen_pdf.node";
import { logger } from "@labor-book/utils/src/log";
import {
  filepath_to_prefix_number_info,
  optimize_outline,
} from "packages/optimize_outline/src";
import package_json from "../../../package.json";
import { map_tree } from "packages/utils/src/tree";
import { ignore_suffix_zero } from "packages/utils/src/parse_prefix_number";
import { _find_xy_for_text, _load_text_position } from "./text_position.node";

/**
 * 打包成一个 PDF 时的上下文对象
 */
export class PackageBookContext {
  constructor(
    public readonly _obj: {
      ctx_main: MainContext;
      ctx_index: IndexingBookContext;
      bookname: string;
      output_pdf_cache_dir_suffixed: string;
    }
  ) {}

  get page_infos() {
    return this._obj.ctx_index._obj.index.pageinfo.map((it, idx) => ({
      ...it,
      num: idx + 1,
    }));
  }

  async insert_outline_pages() {}
}

/**
 * 打包成一个 PDF 的入口函数。
 */
export async function _package_book(ctx: PackageBookContext) {
  const { bookname } = ctx._obj;
  logger.info({ bookname }, "Start package");
  const dst_filepath = `${ctx._obj.ctx_main.dist_dir_suffixed}${bookname}.pdf`;

  await ctx.insert_outline_pages();

  const dst = await pdflib.PDFDocument.create();
  await _slice_by_filepath_then_append({
    dst,
    page_infos: ctx.page_infos,
    ctx,
  });

  const src_filepath_no_outline = `${ctx._obj.output_pdf_cache_dir_suffixed}${ctx._obj.bookname}.nooutline.pdf`;
  await fs.writeFile(src_filepath_no_outline, await dst.save());

  const dst_filepath_with_outline = `${ctx._obj.output_pdf_cache_dir_suffixed}${ctx._obj.bookname}.withoutline.pdf`;

  await _write_outline({
    src_filepath_no_outline,
    res_filepath: dst_filepath_with_outline,
    ctx: ctx,
  });

  const doc = await pdflib.PDFDocument.load(
    await fs.readFile(dst_filepath_with_outline)
  );

  await _do_something_end({
    doc,
    ctx,
  });

  await fs.writeFile(
    dst_filepath,
    await doc.save({
      useObjectStreams: true,
      addDefaultPage: true,
    })
  );

  if (!process.env.IS_GITLAB_CI) {
    // 开发时查看 dist 的 docmeta
    await _init_doc_meta({
      pdf_filepath: dst_filepath,
      output_filepath: `${dst_filepath}.docmeta.pdf.json`,
      mode: "pdf",
    });
  }

  logger.info(
    { bookname, dst_filepath },
    "End package , it will output to file"
  );
}

/**
 * 将 page_infos 按 src_pdf_filepath 分组并将切片 it 传入 _append(dst,it)
 * @param _obj
 */
async function _slice_by_filepath_then_append(_obj: {
  dst: PDFDocument;
  page_infos: typeof PackageBookContext.prototype.page_infos;
  ctx: PackageBookContext;
}) {
  const { ctx, dst, page_infos } = _obj;

  let current_file = "";
  let file_start_page_idx = 0;

  for (let page_idx = 0; page_idx < page_infos.length; page_idx++) {
    const page_info = page_infos[page_idx];
    if (current_file != page_info.src_pdf_filepath) {
      if (file_start_page_idx != page_idx) {
        const { name } = _slice_by_filepath_then_append;
        logger.trace(
          {
            name,
            file_start_page_idx,
            page_idx,
            current_file,
            page_info,
          },
          "copy and write pages"
        );
        await _append({
          dst,
          page_infos: page_infos.slice(file_start_page_idx, page_idx),
        });
        file_start_page_idx = page_idx;
      }
      current_file = page_info.src_pdf_filepath;
    }
  }
  if (file_start_page_idx != page_infos.length) {
    await _append({
      dst,
      page_infos: page_infos.slice(file_start_page_idx, page_infos.length),
    });
  }
}

/**
 * 将同一个源 PDF 文件的各页追加到 dst。
 */
async function _append(_obj: {
  dst: PDFDocument;
  page_infos: typeof PackageBookContext.prototype.page_infos;
}) {
  const { dst, page_infos } = _obj;
  if (page_infos.length == 0) {
    return;
  }
  const src = await pdflib.PDFDocument.load(
    await fs.readFile(page_infos[0].src_pdf_filepath)
  );
  const nums: number[] = [];
  for (let i = 0; i < page_infos.length; i++) {
    nums.push(i);
  }
  const pages = await dst.copyPages(src, nums);
  pages.forEach((it) => dst.addPage(it));
}

/**
 * 写入大纲到 res_filepath
 * @param _obj
 */
export async function _write_outline(_obj: {
  src_filepath_no_outline: string;
  res_filepath: string;
  ctx: PackageBookContext;
}) {
  // I find this from :
  // https://github.com/Hopding/pdf-lib/issues/127
  const { src_filepath_no_outline, res_filepath, ctx } = _obj;
  const res_pdfdoc = await PDFDocument.load(
    await fs.readFile(src_filepath_no_outline)
  );
  const _outline_idx = ctx._obj.ctx_index._obj.index.outline;
  const allow_replace_parent_default = (
    p: OutlineNode<number>,
    c: OutlineNode<number>
  ) => {
    switch (p.payload.t) {
      case "in_page":
        return false;
      default:
        return true;
    }
  };
  const _outline_idx_optimized = await optimize_outline({
    nodes: _outline_idx,
    items_flag: "children",
    hooks: {
      allow_replace_parent___on_shorten_single_branch_treenode: async (p, c) =>
        allow_replace_parent_default(p, c),
      before_replace_to: async (old, cur) => {
        logger.debug(`[before_replace_to] ${old.title} <<< ${cur.title}`);
      },
      before_travel_node: async () => {},
    },
  });
  const _outline_idx_optimized_2 = _outline_idx_optimized.map(
    (node: OutlineNode<number>) =>
      map_tree<"children", OutlineNode<number>, OutlineNodeWithDisplayTitle>({
        node,
        items_flag: "children",
        fn: (n) => {
          // 添加前缀号。
          if (n.payload.t == "in_page" && n.level == 1) {
            const { prefix_numbers } = filepath_to_prefix_number_info(
              n.payload.pdf_filepath
            );
            return {
              ...n,
              display_title: `${ignore_suffix_zero(prefix_numbers).join(".")} ${
                n.title
              }`,
            };
          } else {
            return {
              ...n,
              display_title: n.title,
            };
          }
        },
      })
  );

  const outlines: PDFOutline[] = await Promise.all(
    _outline_idx_optimized_2.map(
      async (outline) =>
        await _outline_node_to_pdf_outline(outline, null, { ctx })
    )
  );
  await setOutline(res_pdfdoc, outlines);
  await fs.writeFile(res_filepath, await res_pdfdoc.save());
}

/**
 * 将自定义的大纲索引节点转为 setOutline 的参数。
 *
 * ---
 *
 * 在这里写下备注，以免自己几个月后看不懂今天写了啥。
 *
 * 这里的解析页码方法是这样的：
 *
 * 1. 当节点为 文件系统目录 时，使用它第一个孩子的页码。
 *
 * 2. 当节点为 文件 时，如果有孩子，寻找它第一个孩子的页码。
 *    如果没孩子，寻找它在页面列表里第一次出现的页面。
 *
 * 3. 当节点为 页面中的标题 时。使用 {@link OutlineNode.payload} 中的信息计算页码和位置。
 */
async function _outline_node_to_pdf_outline(
  node: OutlineNodeWithDisplayTitle,
  parent: OutlineNodeWithDisplayTitle | null,
  _obj: {
    ctx: PackageBookContext;
  }
): Promise<PDFOutline> {
  const { title, display_title } = node;
  try {
    const { ctx } = _obj;
    const { ctx_index } = ctx._obj;

    const _pdf_filepath = node.payload.pdf_filepath;

    const text_position = (await _load_text_position(_pdf_filepath))[
      node.payload.page_num
    ];

    const _get_page_num_start_offset = () => {
      const idx = ctx_index._obj.index.pageinfo.findIndex(
        (it) => it.src_pdf_filepath == _pdf_filepath
      );
      if (idx < 0) {
        throw new Error(
          `not found matched page , where src_pdf_filepath == ${JSON.stringify(
            _pdf_filepath
          )}`
        );
      }
      return idx;
    };

    const get_children = async () =>
      await Promise.all(
        node.children.map(
          async (node_child) =>
            await _outline_node_to_pdf_outline(node_child, node, {
              ctx,
            })
        )
      );

    switch (node.payload.t) {
      case "in_page":
        const start_offset = _get_page_num_start_offset();
        const _page_num_default = start_offset + node.payload.page_num - 1;
        const _to = _find_xy_for_text(text_position, title, _page_num_default);
        if (node.children == null || node.children.length == 0) {
          return {
            title: display_title,
            to: _to,
          };
        } else {
          const children = await get_children();
          return {
            title: display_title,
            to: _to,
            open: true,
            children,
          };
        }
    }
  } catch (err) {
    throw new Error(
      `Failed mapping outline node to pdf outline !
    node   is ${JSON.stringify(node)} ,
    parent is ${JSON.stringify(parent, (k, v) =>
      k == "children" ? "(...)" : v
    )}
    `,
      {
        cause: err,
      }
    );
  }
}

async function _do_something_end(_obj: {
  doc: PDFDocument;
  ctx: PackageBookContext;
}) {
  const { doc, ctx } = _obj;

  doc.setLanguage("zh-CN");
  doc.setProducer(package_json.repository.url);
  doc.addJavaScript("main", 'console.show(); console.println("Hello");');
  const viewer_pref = doc.catalog.getOrCreateViewerPreferences();
  viewer_pref.setDisplayDocTitle(true);
  viewer_pref.setCenterWindow(false);
  viewer_pref.setFitWindow(false);
  viewer_pref.setDuplex(Duplex.DuplexFlipLongEdge);
  viewer_pref.setHideMenubar(false);
  viewer_pref.setHideWindowUI(false);
  viewer_pref.setHideToolbar(false);

  // const acro_form = doc.catalog.getOrCreateAcroForm();

  switch (ctx._obj.bookname) {
    case "health58":
      doc.setAuthor("ismist.cn");
      doc.setCreator("五年八班卫生委员");
      doc.setKeywords(["劳动"]);
      doc.setTitle("健康知识急救手册");
      doc.setSubject("58小书");
      break;
    // case "labor54":
    //   doc.setAuthor("ismist.cn");
    //   doc.setCreator("五年四班劳动委员");
    //   doc.setKeywords(["劳动"]);
    //   doc.setTitle("劳动法学习实操手册");
    //   doc.setSubject("54小书");
    //   break;
    default:
      doc.setAuthor("ismist.cn");
      doc.setCreator("ismist.cn");
      doc.setKeywords([]);
  }

  doc.catalog.Pages().traverse((node, ref) => {});
}
