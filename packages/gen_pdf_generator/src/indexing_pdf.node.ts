import fs from "fs/promises";
import { _exist_pth, _kill_child_subprocess } from "./util.node";
import {
  _if_absent_add_suffix,
  _replace_prefix,
  _replace_suffix,
} from "@labor-book/utils/src/util";
import { logger } from "@labor-book/utils/src/log";
import { MainContext } from "./main.node";
import fg from "fast-glob";
import { _create_indexes_from_pdf_file_docmeta } from "./index.node";
import { filepath_to_prefix_number_info } from "packages/optimize_outline/src";
import { ignore_suffix_zero } from "packages/utils/src/parse_prefix_number";
import lodash from "lodash";

/**
 * 大纲层级
 */
export type OutlineNodeLevel = number;

/**
 * 大纲节点的孩子数组
 */
export type OutlineNodeChildren<Level extends OutlineNodeLevel> = OutlineNode<
  Level extends 1
    ? 2
    : Level extends 2
    ? 3
    : Level extends 3
    ? 4
    : Level extends 4
    ? 5
    : Level extends 5
    ? 6
    : number
>[];

/**
 * 大纲节点的 载荷信息。
 */
export type OutlineNodePayload = {
  t: "in_page";
  page_num: number; // 此值总是 >= 1
  pdf_filepath: string;
  prefix_number: number[];
};

/**
 * 大纲节点
 */
export interface OutlineNode<Level extends OutlineNodeLevel> {
  level: Level;
  id: string;
  title: string;
  payload: OutlineNodePayload;
  children: OutlineNodeChildren<Level>;
}

/**
 * 文档元数据中的 PDF 页信息。
 */
export type DocMetaPdfPage = {
  index: number; // it's always greater or equal 1 .
  page: {
    textContent: {
      items: TextContentItem[];
      styles: {}[];
    };
    structTree?: DocMetaPageStructTreeNode<"Root">;
  };
};

/**
 * 文档元数据
 *
 * load from `**.docmeta.*.json`
 */
export type DocMeta = {
  md?: {};
  pdf: {
    numPages: number;
    outline: any;
    destinations: {
      [k: string]: any;
    };
    pages: DocMetaPdfPage[];
  };
};

/**
 * 文档元数据中 页信息 的 textContent 属性对象。
 */
export type TextContentItem =
  | {
      id: string;
      type: "beginMarkedContentProps";
      tag: "P";
    }
  | {
      str: string;
      dir: "ltr";
      width: number;
      height: number;
      transform: [number, number, number, number, number, number];
      fontName: string;
      hasEOL: boolean;
    }
  | {
      type: "endMarkedContent";
    };

/**
 * 文档元数据中 页信息 的 structTree 属性节点对象 的 role 属性。
 */
export type StructTreeNodeRole =
  | "Root"
  | "NonStruct"
  | "Document"
  | "Div"
  | "P"
  | "H1"
  | "H2"
  | "H3"
  | "H4"
  | "H5"
  | "H6"
  | string;

/**
 * 文档元数据中 页信息 的 structTree 属性节点对象。
 */
export type DocMetaPageStructTreeNode<Role extends StructTreeNodeRole> =
  | {
      role: Role;
      children: DocMetaPageStructTreeNode<string>[];
    }
  | {
      role: undefined;
      type: string;
      id: string;
    };

/**
 * 索引中的 PDF 页信息
 */
export type PageInfo = {
  t: "copy" | "doc";
  src_pdf_filepath: string;
  src_page: number; // always greater or equal then 1 .
};

/**
 * 建立索引时的上下文对象。
 */
export class IndexingBookContext {
  constructor(
    public readonly _obj: {
      index: {
        outline: OutlineNode<1>[];
        pageinfo: PageInfo[];
      };
    } = {
      index: {
        outline: [],
        pageinfo: [],
      },
    }
  ) {}

  private _num_page_current: number = 0;

  /**
   * 写入一页的信息。
   */
  push_page(_p: { page: Readonly<PageInfo> }): {
    page_num: number;
  } {
    const { page } = _p;
    this._obj.index.pageinfo.push(page);
    this._num_page_current += 1;
    return {
      page_num: this._num_page_current,
    };
  }

  /**
   * 写入大纲信息。
   *
   * @param paths 标题层级数组。
   */
  push_index(paths: Readonly<IndexingBookContextPusherPaths>): void {
    // if (paths.length > 6) {
    //   throw Error(`Invalid param . Arguments were ${arguments}`);
    // }
    // let nodes = this._obj.index.outline as OutlineNode<OutlineNodeLevel>[];
    // for (let i = 0; i < paths.length; i++) {
    //   const path = paths[i];
    //   let node_target: ArrayElement<typeof nodes>;
    //   const nodes_id_matched = nodes.filter((it) => it.id == path.id);
    //   if (nodes_id_matched.length > 1) {
    //     throw Error(
    //       `Duplicate id ! path is ${path} , paths were ${paths} ,nodes were ${JSON.stringify(
    //         nodes,
    //         null,
    //         2
    //       )}`
    //     );
    //   } else if (nodes_id_matched.length == 0) {
    //     if (!path.title) {
    //       throw Error(`Untitled ! path is ${path} , paths were ${paths}`);
    //     }
    //     node_target = {
    //       id: path.id,
    //       title: path.title,
    //       level: (i + 1) as OutlineNodeLevel,
    //       _pdf_info: path._pdf_info,
    //       children: [],
    //     };
    //     nodes.push(node_target);
    //   } else {
    //     node_target = nodes_id_matched[0];
    //   }
    //   nodes = node_target.children as OutlineNode<OutlineNodeLevel>[];
    // }
  }
}

/**
 * 索引上下文对象 写入大纲信息 的 标题层级数组 参数类型。
 */
export type IndexingBookContextPusherPaths = {
  id: string;
  title?: string;
  _pdf_info: OutlineNodePayload;
}[];

/**
 * 入口方法。
 *
 * @param ctx_main 全局上下文。
 * @param ctx 索引上下文。
 * @param bookname 书的目录名。
 * @param output_pdf_cache_dir_suffixed 缓存目录名，带 / 后缀。
 */
export async function _indexing_book(
  ctx_main: MainContext,
  ctx: IndexingBookContext,
  bookname: string,
  output_pdf_cache_dir_suffixed: string
) {
  logger.info(
    {
      bookname,
      output_pdf_cache_dir_suffixed,
    },
    "Book indexing start"
  );
  const pdf_filepaths = (
    await fg(`${output_pdf_cache_dir_suffixed}**/*.pdf`)
  ).sort((a, b) =>
    _book_pdf_filepath_sorter(a, b, {
      output_pdf_cache_dir_suffixed,
    })
  );

  if (pdf_filepaths.length == 0) {
    throw Error("Not found pdf file");
  }

  for (const _pdf_filepath of pdf_filepaths) {
    const pdf_filepath_relative = _replace_prefix(
      _pdf_filepath,
      output_pdf_cache_dir_suffixed,
      ""
    );
    await _append_indexes_from_pdf_file({
      ctx_main,
      ctx,
      _pdf_filepath,
      output_pdf_cache_dir_suffixed,
      allow_not_found: ["cover.pdf"].includes(pdf_filepath_relative)
        ? true
        : false,
      mode: pdf_filepath_relative.endsWith(".copy.pdf") ? "copy" : "doc",
    });
  }

  const index_output_file = `${output_pdf_cache_dir_suffixed}${ctx_main.cache_index_file_relative_path}`;
  logger.debug(
    {
      index_output_file,
    },
    "Book index created , it will output to file"
  );
  await fs.writeFile(
    index_output_file,
    JSON.stringify(ctx._obj.index, null, 2)
  );
}

/**
 * 将一个 PDF 文件 解析并写入索引。
 * @param params
 */
async function _append_indexes_from_pdf_file(params: {
  ctx_main: MainContext;
  ctx: IndexingBookContext;
  _pdf_filepath: string;
  output_pdf_cache_dir_suffixed: string;
  allow_not_found: boolean;
  mode: "copy" | "doc";
}) {
  const {
    ctx_main,
    ctx,
    _pdf_filepath,
    allow_not_found,
    output_pdf_cache_dir_suffixed,
  } = params;
  if (!_exist_pth(_pdf_filepath) && !allow_not_found) {
    throw Error(`not found pdf file : ${_pdf_filepath}`);
  }
  if (params.mode == "copy") {
    const docmeta = await _load_docmeta(_pdf_filepath);
    for (let i = 1; i <= docmeta.pdf.numPages; i++) {
      ctx.push_page({
        page: {
          t: "copy",
          src_pdf_filepath: _pdf_filepath,
          src_page: i,
        },
      });
    }
  } else if (params.mode == "doc") {
    logger.trace(
      {
        _pdf_filepath,
      },
      "indexing document"
    );
    await _append_indexes_from_pdf_file_which_mode_doc({
      _pdf_filepath,
      ctx,
      ctx_main,
      output_pdf_cache_dir_suffixed,
    });
  } else {
    throw Error();
  }
}

/**
 * 从json文件中读取以前解析完成的元数据。
 * @param pdf_filepath PDF 文件路径。
 * @returns
 */
export async function _load_docmeta(pdf_filepath: string): Promise<DocMeta> {
  const docmeta = {} as DocMeta;
  const md_json = `${pdf_filepath}.docmeta.md.json`;
  if (await _exist_pth(md_json)) {
    docmeta.md = JSON.parse(await fs.readFile(md_json, "utf-8"));
  }
  const pdf_json = `${pdf_filepath}.docmeta.pdf.json`;
  if (await _exist_pth(pdf_json)) {
    docmeta.pdf = JSON.parse(await fs.readFile(pdf_json, "utf-8"));
  }
  const check_result = _check_docmeta(docmeta);
  if (!check_result.valid) {
    throw check_result.error;
  }
  return docmeta;
}

function _check_docmeta(docmeta: DocMeta):
  | {
      valid: true;
    }
  | {
      valid: false;
      error: Error;
    } {
  if (docmeta.pdf.pages.length != docmeta.pdf.numPages) {
    return {
      valid: false,
      error: new Error(
        `pdf.pages.length (${docmeta.pdf.pages.length}) not equal pdf.numPages (${docmeta.pdf.numPages})`
      ),
    };
  }
  return {
    valid: true,
  };
}

/**
 * 决定所有文档排列顺序的比较器。
 */
function _book_pdf_filepath_sorter(
  a: string,
  b: string,
  option: {
    output_pdf_cache_dir_suffixed: string;
  }
): number {
  const a2 = _replace_prefix(a, option.output_pdf_cache_dir_suffixed, "").split(
    "/"
  );
  const b2 = _replace_prefix(b, option.output_pdf_cache_dir_suffixed, "").split(
    "/"
  );
  const len_min = Math.min(a2.length, b2.length);
  const _filename_without_suffixes = (n: string) =>
    n.indexOf(".") > 0 ? n.substring(0, n.indexOf(".")) : n;
  for (let i = 0; i < len_min; i++) {
    const a3 = _filename_without_suffixes(a2[i]);
    const b3 = _filename_without_suffixes(b2[i]);
    if (a3 == "cover") return -1;
    if (b3 == "cover") return 1;
    const cmp_res = a3.localeCompare(b3);
    if (cmp_res != 0) return cmp_res;
  }
  return a2.length == b2.length ? 0 : a2.length < b2.length ? -1 : 1;
}

/**
 * 将 一个视为“文档”的 PDF 文件 及其父目录们 解析并写入索引。
 */
async function _append_indexes_from_pdf_file_which_mode_doc(params: {
  _pdf_filepath: string;
  ctx: IndexingBookContext;
  ctx_main: MainContext;
  output_pdf_cache_dir_suffixed: string;
}) {
  const { _pdf_filepath, ctx, ctx_main, output_pdf_cache_dir_suffixed } =
    params;
  const docmeta = await _load_docmeta(_pdf_filepath);

  // const pdf_routing_path_splited = _replace_prefix(
  //   _pdf_filepath,
  //   output_pdf_cache_dir_suffixed,
  //   ""
  // ).split("/");

  await __append_indexes_from_pdf_file_which_mode_doc({
    current_level: 1,
    parent_node: null,
    ctx,
    ctx_main,
    // pdf_routing_path_splited,
    _pdf_filepath,
    output_pdf_cache_dir_suffixed,
    docmeta,
  });
}

async function __append_indexes_from_pdf_file_which_mode_doc(_obj: {
  current_level: OutlineNodeLevel;
  parent_node: null | OutlineNode<typeof _obj.current_level>;
  ctx: IndexingBookContext;
  ctx_main: MainContext;
  // pdf_routing_path_splited: readonly string[];
  _pdf_filepath: string;
  output_pdf_cache_dir_suffixed: string;
  docmeta: DocMeta;
}) {
  const {
    parent_node,
    ctx,
    ctx_main,
    current_level,
    // pdf_routing_path_splited,
    _pdf_filepath,
    output_pdf_cache_dir_suffixed,
    docmeta,
  } = _obj;
  const nodes_in_my_level =
    parent_node == null ? ctx._obj.index.outline : parent_node.children;

  if (nodes_in_my_level == null) {
    throw new Error(
      "BUG , stack trace arguments : " + JSON.stringify(arguments, null, 2)
    );
  }
  let target_nodes = nodes_in_my_level;
  const prefix_number = ignore_suffix_zero(
    filepath_to_prefix_number_info(_pdf_filepath).prefix_numbers
  );
  for (let i = 0; i < prefix_number.length; i++) {
    const sliced_prefix_number = prefix_number.slice(0, i + 1);
    const existed = target_nodes.findLast((it) =>
      lodash.isEqual(it.payload.prefix_number, sliced_prefix_number)
    );
    if (existed == undefined) {
      break;
    } else {
      target_nodes = existed.children;
    }
  }
  const indexes = await _create_indexes_from_pdf_file_docmeta({
    current_level,
    docmeta,
    _pdf_filepath,
    prefix_number,
    on_pdf_page: (pdf_page) =>
      ctx.push_page({
        page: {
          t: "doc",
          src_page: pdf_page.index,
          src_pdf_filepath: _pdf_filepath,
        },
      }),
  });
  target_nodes.push(...indexes.nodes);
}
