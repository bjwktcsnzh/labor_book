import fs from "fs/promises";
import { _exist_pth, _kill_child_subprocess } from "./util.node";
import { _generate_pdf } from "./gen_pdf.node";
import {
  _start_preview_server,
  _stop_preview_server,
} from "./preview_server.node";
import { _if_absent_add_suffix } from "@labor-book/utils/src/util";
import { logger } from "@labor-book/utils/src/log";

// https://github.com/condorheroblog/vitepress-export-pdf/blob/main/src/serverApp.ts
//
// 参考 :
//
export async function main(ctx_main: MainContext) {
  logger.info("Start to generate pdf ...");

  await fs.mkdir(ctx_main.dist_dir_suffixed, { recursive: true });

  if (await _exist_pth(ctx_main.cache_dir_suffixed)) {
    await fs.rm(ctx_main.cache_dir_suffixed, { recursive: true });
  }

  await fs.mkdir(ctx_main.cache_dir_suffixed, { recursive: true });

  const preview_server = await _start_preview_server(ctx_main);
  try {
    for (const bookname of ctx_main._obj.booknames) {
      await _generate_pdf(ctx_main, bookname, "before_package");
    }
  } finally {
    await _stop_preview_server("preview_server", preview_server);
  }
  logger.info(`
  +--------------------------+
  |                          |
  |         Success          |
  |                          |
  +--------------------------+
  `)
}

export class MainContext {
  constructor(
    public _obj: {
      readonly docs_dir: string;
      readonly dist_dir: string;
      readonly cache_dir: string;
      readonly cache_index_file_relative_path: string;
      readonly styles_dir: string;
      readonly booknames: readonly string[];
      readonly preview_server: {
        readonly port: number;
      };
      readonly is_test: boolean;
    }
  ) {}

  get docs_dir_suffixed() {
    return _if_absent_add_suffix(this._obj.docs_dir, "/");
  }

  get dist_dir_suffixed() {
    return _if_absent_add_suffix(this._obj.dist_dir, "/");
  }

  get cache_dir_suffixed() {
    return _if_absent_add_suffix(this._obj.cache_dir, "/");
  }

  get cache_index_file_relative_path() {
    return this._obj.cache_index_file_relative_path;
  }

  get styles_dir_suffixed() {
    return _if_absent_add_suffix(this._obj.styles_dir, "/");
  }
}
