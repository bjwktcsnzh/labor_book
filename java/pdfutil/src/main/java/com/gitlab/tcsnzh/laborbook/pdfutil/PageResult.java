package com.gitlab.tcsnzh.laborbook.pdfutil;

import java.util.ArrayList;

public record PageResult(int page, float w, float h, ArrayList<TextPosResult> texts) {
}
