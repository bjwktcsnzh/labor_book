package com.gitlab.tcsnzh.laborbook.pdfutil;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.pdfbox.Loader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            throw new IllegalArgumentException("require one argument of PDF file path .");
        }
        var path = args[0];
        var output = new File(path + ".docmeta.text_position.json");
        try (var doc = Loader.loadPDF(new File(path))) {

//            var obj = doc.getDocumentCatalog().getOCProperties();
//            System.out.println(new ObjectMapper()
//                    .disable(SerializationFeature.FAIL_ON_SELF_REFERENCES)
//                    .enable(SerializationFeature.WRITE_SELF_REFERENCES_AS_NULL)
//                    .writerWithDefaultPrettyPrinter()
//                    .writeValueAsString(obj));


            var pts = new MyPDFTextStripper();
            @SuppressWarnings("unused") var text = pts.getText(doc);

//            var result = new HashMap<Integer, PageResult>();
//            for (MyTextInfo tp : pts.posList) {
//                var r = result.computeIfAbsent(tp.page(), it ->
//                        new PageResult(it, tp.pageW(), tp.pageH(), new ArrayList<>()));
//                if (Math.abs(tp.pageW() - r.w()) > 1 || Math.abs(tp.pageH() - r.h()) > 1) {
//                    throw new IllegalStateException("page size not match , tp=" + tp + " , r=" + r);
//                }
//                r.texts().add(new TextPosResult(tp.text(), tp.x(), tp.y()));
//            }
            new ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(new FileOutputStream(output), pts.resultList);
        }
    }
}
