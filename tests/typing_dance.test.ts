import { TestOptions, expect, test, assertType } from "vitest";
import { _has_property } from "../packages/utils/src/typed";

test("has", () => {
  const o1 = { a: 100, b: "B" };
  if (_has_property(o1, "a")) {
    assertType<number>(o1.a);
  }
  if (_has_property(o1, "x")) {
    assertType<unknown>(o1.x);
  }
  if (_has_property(o1, "y")) {
    assertType<unknown>(o1.y);
  }
  const the_a = "a" as string;
  if (_has_property(o1, the_a)) {
    assertType<unknown>(o1[the_a]);
  }
});
