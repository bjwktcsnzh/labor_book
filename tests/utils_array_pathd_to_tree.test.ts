import { expect, test } from "vitest";
import {
  array_pathd_to_tree,
  array_pathd_to_tree_0,
} from "@labor-book/utils/src/array_pathd_to_tree";

test("array_pathd_to_tree", async () => {
  let paths = [
    "About.vue",
    "Categories/Index.vue",
    "Categories/Demo.vue",
    "Categories/Flavors.vue",
    "Categories/Types/Index.vue",
    "Categories/Types/Other.vue",
  ];

  const res = await array_pathd_to_tree_0(
    "path",
    paths.map((path) => ({ path }))
  );

  const expect_res = [
    {
      path_item: "About.vue",
      items: [],
      param: {
        path: "About.vue",
      },
    },
    {
      path_item: "Categories",
      items: [
        {
          path_item: "Index.vue",
          items: [],
          param: {
            path: "Categories/Index.vue",
          },
        },
        {
          path_item: "Demo.vue",
          items: [],
          param: {
            path: "Categories/Demo.vue",
          },
        },
        {
          path_item: "Flavors.vue",
          items: [],
          param: {
            path: "Categories/Flavors.vue",
          },
        },
        {
          path_item: "Types",
          items: [
            {
              path_item: "Index.vue",
              items: [],
              param: {
                path: "Categories/Types/Index.vue",
              },
            },
            {
              path_item: "Other.vue",
              items: [],
              param: {
                path: "Categories/Types/Other.vue",
              },
            },
          ],
        },
      ],
    },
  ];

  expect(res).toStrictEqual(expect_res);
});

test("array_pathd_to_tree rename param", async () => {
  let paths = [
    "About.vue",
    "Categories/Index.vue",
    "Categories/Demo.vue",
    "Categories/Flavors.vue",
    "Categories/Types/Index.vue",
    "Categories/Types/Other.vue",
  ];

  const res2 = await array_pathd_to_tree({
    split_char: "/",
    params: paths.map((path) => ({ filePath: path })),
    flags: {
      path: "filePath",
      path_item: "text",
      items: "children",
      param: "node",
    },
    hooks: {
      onCreateMapping: async (_arg) => {
        const { param, path_item, is_param_end } = _arg;
        return {
          // filePath: param.filePath,
          text: path_item,
          children: [],
          ...(is_param_end ? { node: param } : {}),
        };
      },
      afterMountValue: async () => {},
    },
  });

  const expect_res2 = [
    {
      text: "About.vue",
      children: [],
      node: {
        filePath: "About.vue",
      },
    },
    {
      text: "Categories",
      children: [
        {
          text: "Index.vue",
          children: [],
          node: {
            filePath: "Categories/Index.vue",
          },
        },
        {
          text: "Demo.vue",
          children: [],
          node: {
            filePath: "Categories/Demo.vue",
          },
        },
        {
          text: "Flavors.vue",
          children: [],
          node: {
            filePath: "Categories/Flavors.vue",
          },
        },
        {
          text: "Types",
          children: [
            {
              text: "Index.vue",
              children: [],
              node: {
                filePath: "Categories/Types/Index.vue",
              },
            },
            {
              text: "Other.vue",
              children: [],
              node: {
                filePath: "Categories/Types/Other.vue",
              },
            },
          ],
        },
      ],
    },
  ];

  expect(res2).toStrictEqual(expect_res2);
});
