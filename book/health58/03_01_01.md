---
internal58:
    authors:
    - 舒豁爽
    reviewers:
    - 舒豁爽
---

# 急性肺栓塞

:::info 名词解释
急性肺栓塞是各种栓子（栓塞时阻塞血管的物质）阻塞肺动脉系统引起的以肺循环和呼吸功能障碍为主要表现的一组疾病或临床综合征的总称，包括肺血栓栓塞、脂肪栓塞、羊水栓塞、空气栓寒。临床上以肺血栓栓塞最为常见。
:::

## 常见临床表现

当患者有不能解释的呼吸困难、胸痛、咳嗽,同时存在深静脉血栓的高危因素，应高度怀疑急性肺栓塞的可能。主要表现为胸痛、咯血、患者甚至有可能出现休克。

## 急救措施

除上一节[呼吸困难中我们可以采取的措施](03_01_00.md#急救措施)以外，我们还应该注意：

1. 如果您感到呼吸短促、胸部不适或胸痛、头晕或晕厥，请立即就医或呼叫救护车。
2. 同时躺下，不要四处走动，不要步行或开车去医院或诊所。
