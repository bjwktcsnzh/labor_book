import package_json from "../../package.json";

export const homepage = package_json.homepage;
export const version = package_json.version;
