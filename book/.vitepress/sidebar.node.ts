import fg from "fast-glob";
import {
  _if_absent_remove_prefix,
  _replace_prefix,
  find_title_in_markdown_content,
  longest_common_prefix,
} from "../../packages/utils/src/util";
import { array_pathd_to_tree } from "../../packages/utils/src/array_pathd_to_tree";
import matter from "gray-matter";
import fs from "fs/promises";
import {
  filepath_to_prefix_number_info,
  optimize_outline,
} from "../../packages/optimize_outline/src/index";
import { DefaultTheme } from "vitepress/theme";
import { map_tree } from "../../packages/utils/src/tree";
import { logger } from "../../packages/utils/src/log";
import { ignore_suffix_zero } from "../../packages/utils/src/parse_prefix_number";

export interface SidebarTreeNodeParam {
  filepath: string;
  logicpath: string;
}

export interface SidebarTreeNode extends DefaultTheme.SidebarItem {
  __param: SidebarTreeNodeParam;
  __is_param_end: boolean;
  __path_item: string;
  items: SidebarTreeNode[];
}

export async function _common_sidebar(_obj: {
  docs_dir_suffixed: string;
  bookname: string;
}): Promise<DefaultTheme.SidebarItem[]> {
  const { docs_dir_suffixed, bookname } = _obj;
  logger.info({ bookname }, "Build sidebar for book");

  const docs_paths = await fg(`${docs_dir_suffixed}${bookname}/**/*.md`);
  const paths_common_prefix = longest_common_prefix(docs_paths);
  const params = docs_paths
    .map((filepath) => ({
      filepath,
      logicpath: ignore_suffix_zero(
        filepath_to_prefix_number_info(
          _replace_prefix(filepath, paths_common_prefix, "")
        ).prefix_numbers
      ).join("/"),
    }))
    .sort();
  const nodes = await array_pathd_to_tree({
    params,
    split_char: "/",
    flags: {
      path: "logicpath",
      path_item: "__path_item",
      items: "items",
      param: "__param",
    },
    hooks: {
      onCreateMapping: async function ({ param, path_item, is_param_end }) {
        const treenode: SidebarTreeNode = {
          __param: param,
          __is_param_end: is_param_end,
          __path_item: path_item,
          items: [],
          text: path_item,
        };
        return treenode;
      },
      afterMountValue: async function (node, brother_nodes) {
        const filepath = node.__param.filepath;
        node.link = _replace_prefix(
          filepath,
          `${docs_dir_suffixed}${bookname}/`,
          `/${bookname}/`
        );
        try {
          const frontmatter = matter(await fs.readFile(filepath, "utf-8"));
          const title = find_title_in_markdown_content(frontmatter.content);
          node.text = `${title}`;
        } catch (err) {
          logger.error(
            {
              filepath,
              err,
            },
            "Failed on after mount value"
          );
          throw err;
        }
      },
    },
  });
  const nodes_optimized = await optimize_outline({
    nodes,
    items_flag: "items",
    hooks: {
      allow_replace_parent___on_shorten_single_branch_treenode: async () =>
        false,
      before_replace_to: async (old, cur) => {
        logger.debug(
          `[before_replace_to] ${old.__param.filepath} <<< ${cur.__param.filepath}`
        );
      },
      before_travel_node: async () => {},
    },
  });
  const nodes_titled_bookname = nodes_optimized.map((node_optimized) =>
    map_tree<"items", SidebarTreeNode, SidebarTreeNode>({
      node: node_optimized,
      items_flag: "items",
      fn(n) {
        if (!n.text) {
          logger.warn({ n }, "No title !");
        }
        const { prefix_numbers } = filepath_to_prefix_number_info(
          n.__param.filepath
        );
        const res = {
          ...n,
        };
        res.text = `${ignore_suffix_zero(prefix_numbers).join(".")} ${n.text}`;
        return res;
      },
    })
  );
  logger.debug(
    {
      // params,
      // nodes,
      // nodes_optimized,
      // nodes_titled_bookname,
    },
    "Sidebar build"
  );
  return nodes_titled_bookname;
}
