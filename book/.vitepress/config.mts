import { defineConfig } from "vitepress";
import { _common_sidebar } from "./sidebar.node.js";
import { _require_home_action, get_home_info } from "./home.node.ts";
import package_json from "../../package.json";
import fs from "fs/promises";
import { _replace_suffix } from "../../packages/utils/src/util.ts";
const { home_title, home_description, home_actions } = await get_home_info({
  docs_dir_suffixed: "./book/",
});
const health58 = _require_home_action("health58", home_actions);

//
// https://vitepress.dev/reference/site-config
//
// Run in Node.js env
//
export default defineConfig({
  lang: "zh-CN",
  title: home_title,
  description: home_description,
  vite: {
    configFile: "./vite.config.ts",
  },
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: "主页", link: "/" },
      {
        text: health58.text,
        link: health58.link,
      },
    ],

    lastUpdated: {
      text: "最后更新于 ",
      formatOptions: {
        dateStyle: "full",
        timeStyle: "medium",
      },
    },

    editLink: {
      pattern: `${_replace_suffix(
        package_json.repository.url,
        ".git",
        ""
      )}/edit/main/book/:path`,
      text: "在 Gitlab 编辑",
    },
    darkModeSwitchLabel: "暗色模式",
    sidebarMenuLabel: "目录",
    returnToTopLabel: "回到顶部",
    langMenuLabel: "语言",
    notFound: {
      title: "页面不见了",
      quote: "或许是文件发生了更改而导致链接失效...",
      linkText: "返回首页",
    },
    // footer: {
    //   message: "你好",
    //   copyright: "世界",
    // },
    outline: {
      label: "大纲",
      level: "deep",
    },
    sidebar: [
      {
        text: "Markdown 语法教程",
        collapsed: true,
        items: await _common_sidebar({
          docs_dir_suffixed: "./book/",
          bookname: "markdown_examples",
        }),
      },
      {
        text: health58.text,
        collapsed: true,
        items: await _common_sidebar({
          docs_dir_suffixed: "./book/",
          bookname: "health58",
        }),
      },
    ],

    socialLinks: [
      {
        icon: {
          svg: await fs.readFile("./book/.vitepress/assets/git.svg", {
            encoding: "utf-8",
          }),
        },
        link: package_json.repository.url,
        ariaLabel: "Git",
      },
    ],
  },
});
