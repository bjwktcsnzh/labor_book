---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "劳动者的魔法书"
  tagline: "这是一本劳动者的魔法书。"
  actions:
    - id: health58
      theme: brand
      text: 健康知识学习实操手册
      link: /health58/01_00_00
    # - theme: alt
    #   text: 参与编写
    #   link: /markdown_examples/readme
# features:
#   - title: Feature A
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#   - title: Feature B
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
#   - title: Feature C
#     details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---
<script setup lang="ts">
import HomePage from './HomePage.vue'
</script>

<HomePage />
