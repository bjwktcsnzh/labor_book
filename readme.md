# Labor Book

## Description

## License 许可证 | 版权声明

> 本节所说的 **文档** 是指：
>
> - 本工程中所有以 `.md`、`.pdf` 为后缀名的文件。
> - 本工程中所有的 图片、视频 格式的文件。

此工程中的 **文档** 使用 

- [ ] TODO ...

此工程的软件源代码（即本工程中所有不为 **文档** 的文件）使用 AGPL 3.0 许可证。

> GNU Affero 通用公共许可证的目的是保证您分享和改变一个程序的所有版本的自由--确保它对所有用户都是自由软件。
>
> 此许可证的英文原件位于: https://www.gnu.org/licenses/agpl-3.0.zh-cn.html
>
> 此许可证的中文译本位于: https://www.chinasona.org/gnu/agpl-3.0-cn.html
>
> [去百度搜索 AGPL 3.0 许可证](https://www.baidu.com/s?wd=%E4%BB%80%E4%B9%88%E6%98%AFAGPL%E8%AE%B8%E5%8F%AF%E8%AF%81)

## 使用方式

在 [Gitlab Page](https://labor-book-tcsnzh-db51f1bb3e4c6d314b959b20a2f8ee686d4cb7316ea7c.gitlab.io/) 上查看本书:

## 贡献方式

### 贡献文档

- [ ] TODO ...

### 贡献代码

想开始贡献代码嘛？请参考 [readme_dev.md](./readme_dev.md) :

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
