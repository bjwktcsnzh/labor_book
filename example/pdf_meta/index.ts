import { getDocument } from "pdfjs-dist";
import { _read_pdf_docmeta } from "@labor-book/gen-pdf-generator/src/gen_pdf.node";
import fs from "fs/promises";
import { PDFDocument } from "pdf-lib";
import path from "path";
import { _exist_pth } from "@labor-book/gen-pdf-generator/src/util.node";

async function _main() {
  const pdf_filepath = "example/pdf_meta/劳动法学习实操手册正式第2版.pdf";
  const docmeta_pdf = await _read_pdf_docmeta(
    await getDocument(pdf_filepath).promise,
    await PDFDocument.load(await fs.readFile(pdf_filepath))
  );
  const out = path.join(
    path.resolve(pdf_filepath, ".."),
    "dist",
    `${path.basename(pdf_filepath)}.docmeta.pdf.json`
  );
  if (!(await _exist_pth(path.resolve(out, "..")))) {
    await fs.mkdir(path.resolve(out, ".."), {
      recursive: true,
    });
  }
  await fs.writeFile(out, JSON.stringify(docmeta_pdf, null, 2));
}

await _main();
