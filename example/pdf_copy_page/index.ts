import pdflib from "pdf-lib";
import fs from "fs/promises";

async function copy_page_example() {
  console.log("copy page example");
  const dst = await pdflib.PDFDocument.create();
  console.log("dst created");
  const src = await pdflib.PDFDocument.load(
    await fs.readFile("./example/pdf_copy_page/cover.pdf")
  );
  console.log("src created");
  const pages = await dst.copyPages(src, [0]);
  console.log("dst page copied");
  dst.addPage(pages[0]);
  console.log("dst added");
  await fs.writeFile("./example/pdf_copy_page/dist/copy_page_example.pdf", await dst.save());
  console.log("ok");
}

copy_page_example();
